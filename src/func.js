const getSum = (str1, str2) => {

    if (typeof(str1) != "string" || typeof(str2) != "string")
        return false;

    if (isNaN(Number(str1)) || isNaN(Number(str2)))
        return false;

    return (Number(str1) + Number(str2)).toString();

};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {

    let posts = 0;
    let comments = 0;

    for (let key of listOfPosts) {

        if (key.author == authorName)
            posts++;


        if (key.hasOwnProperty("comments")) {

            for (const keyy in key.comments) {

                if (key.comments[keyy].author == authorName) {
                    comments++;
                }

            }
        }
    }
    return `Post:${posts},comments:${comments}`;
};

const tickets = (people) => {

    let sum = 0;
    for (const element of people) {
        if (element == 25) {
            sum += element;
        } else {
            if (element - 25 > sum) return "NO";
            sum += 25;
            sum -= element - 25;
        }
    }
    return "YES";
};


module.exports = { getSum, getQuantityPostsByAuthor, tickets };